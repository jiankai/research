// C program for array implementation of queue
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"


// Driver program to test above functions./
int main()
{
    struct Queue* queue = createQueue(1000);

    char **arrayOfString;
      arrayOfString = malloc(1000 * sizeof(char*));
      for(int i = 0; i < 1000; i++){
        arrayOfString[i] = malloc(BUFFER_SIZE);
      }
    // for(int i=0;i<1000;i++) {
    //   snprintf(arrayOfString[i], 100, "can_id: %d XXXXXXXXXXXXXXXXXXXXXXXxxxx",i);
    // }
    crete_array_string_queue(0, arrayOfString);
    print_array_string(arrayOfString,1000);

    enqueue(queue, arrayOfString);
    crete_array_string_queue(1, arrayOfString);
    enqueue(queue, arrayOfString);
    //print_array_queue(queue,0,1000);
    //print_array_queue(queue,1,1000);
    // struct Data front1 = front(queue);
    // struct Data rear1 = rear(queue);
    // for(int i =0;i<1000;i++) {
    //   printf("%s\n", front1.listOfString[i] );
    // }
    printf("size: %d\n",queue_size(queue) );

    struct Data current = dequeue(queue);
    current = dequeue(queue);
    for(int i =0;i<1000;i++) {
      printf("%s\n", current.listOfString[i] );
    }
    printf("size: %d\n",queue_size(queue) );
    // char **arrayOfString;
    // arrayOfString = malloc(1000 * sizeof(char*));
    // for(int i = 0; i < 1000; i++){
    //   arrayOfString[i] = malloc(BUFFER_SIZE);
    // }
    // for(int i=0;i<1000;i++) {
    //   snprintf(arrayOfString[i] 100, "can_id: %d XXXXXXXXXXXXXXXXXXXXXXXxxxx",i)
    // }
    // enqueue(queue, 10);
    // enqueue(queue, 20);
    // enqueue(queue, 30);
    // enqueue(queue, 40);
    //
    // printf("%d dequeued from queue\n\n",
    //        dequeue(queue));
    //
    // printf("Front item is %d\n", front(queue));
    // printf("Rear item is %d\n", rear(queue));

    return 0;
}
