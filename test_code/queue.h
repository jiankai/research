// // C program for array implementation of queue
// #include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 1024

struct Data {
  char *listOfString[1000];
  // char listOfString[1000][100];
};
// A structure to represent a queue
struct Queue {
    int front, rear, size;
    unsigned capacity;
    struct Data *array; // array of string (char)
};

// function to create a queue of given capacity.
// It initializes size of queue as 0
struct Queue* createQueue(unsigned capacity)
{
    struct Queue* queue = (struct Queue*)malloc(sizeof(struct Queue));
    queue->capacity = capacity;
    queue->front = queue->size = 0;

    // This is important, see the enqueue
    queue->rear = capacity - 1;

    queue->array = (struct Data*)malloc(
        queue->capacity * sizeof(struct Data));
    return queue;
}

// Queue is full when size becomes
// equal to the capacity
int isFull(struct Queue* queue)
{
    return (queue->size == queue->capacity);
}

// Queue is empty when size is 0
int isEmpty(struct Queue* queue)
{
    return (queue->size == 0);
}

int queue_size(struct Queue* queue) {
  return queue->size;
}

// Function to add an item to the queue.
// It changes rear and size
void enqueue(struct Queue* queue, char *item[])
{
    if (isFull(queue))
        return;
    queue->rear = (queue->rear + 1)
                  % queue->capacity;

    //queue->array[queue->rear] = item;
    for(int i=0;i<1000;i++){
      // strcpy()
      queue->array[queue->rear].listOfString[i] = item[i];
    }
    queue->size = queue->size + 1;
    printf("Performed enqueue\n" );
    // printf("%d enqueued to queue\n", item);
}

// Function to remove an item from queue.
// It changes front and size
struct Data dequeue(struct Queue* queue)
{
  struct Data data;
    if (isEmpty(queue))
        return data;
    struct Data item = queue->array[queue->front];
    queue->front = (queue->front + 1)
                   % queue->capacity;
    queue->size = queue->size - 1;
    printf("Performed Dequeue \n" );
    return item;
}

// Function to get front of queue
struct Data front(struct Queue* queue)
{
    struct Data data;
    if (isEmpty(queue))
        return data;
    return queue->array[queue->front];
}

// Function to get rear of queue
struct Data rear(struct Queue* queue)
{
    struct Data data;
    if (isEmpty(queue))
        return data;
    return queue->array[queue->rear];
}



void print_array_string(char *item[], int size) {
  for(int i=0;i < size; i++) {
    printf("%s\n",item[i] );
  }
}

void print_array_queue(struct Queue* queue,int index,int size) {
  printf("list in queue\n" );
  for(int i=0;i< size; i++) {
    printf("%d %s\n", index, queue->array[index].listOfString[i] );
  }
}
void crete_array_string_queue(int index, char* arrayOfString[]) {
  for(int i=0;i<1000;i++) {
    snprintf(arrayOfString[i], 100, "%d can_id: %d XXXXXXXXXXXXXXXXXXXXXXXxxxx",index,i);
  }
}
