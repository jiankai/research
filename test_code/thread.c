#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t count_mutex;
int shared_count = 0;
struct data {
    long id;
    int number;
};

void *workerThreadFunc(void *input) {
  pthread_mutex_lock(&count_mutex);
  long id = ((struct data*)input)->id;
  int number = ((struct data*)input)->number;
  printf("Hello world! this is thread %ld -- %d\n", id , number);
  printf("id: %d -- count: %d\n",number,shared_count );
  sleep(1);
  printf("End! this is thread %ld -- %d\n", id , number);
  shared_count++;
  pthread_mutex_unlock(&count_mutex);
}

int main(){

  pthread_t tid0;
  pthread_t tid1;
  pthread_t tid2;
  pthread_t *pthreads[] = {&tid0, &tid1, &tid2};
  struct data *thread_list[3];
  for(int i=0;i< 3;i++) {
    printf("thread id: %ld\n", *pthreads[i]);
  }
  for(int i=0;i < 3;i++) {
    thread_list[i] = (struct data *)malloc(sizeof(struct data));
    thread_list[i] -> id = *pthreads[i];
    thread_list[i] -> number = i;
  }
  // struct data *thread_list = (struct data *)malloc(sizeof(struct data));

  for(int i=0;i < 3; i++) {
    pthread_create(pthreads[i], NULL, workerThreadFunc, (void *)thread_list[i]);
  }
  //pthread_create(&tid0, NULL, workerThreadFunc, (void *)&tid0);
  pthread_exit(NULL);
  return 0;
}
