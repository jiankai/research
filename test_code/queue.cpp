#include <bits/stdc++.h>
#include <string>
#include <queue>
using namespace std;

struct QNode {
	array<string, 1000> data;
	QNode* next;
	QNode(array<string, 1000> d)
	{
		data = d;
		next = NULL;
	}
};

struct Queue {
	QNode *front, *rear;
  int count;
  /**
   * Constructor
   */
	Queue()
	{
		front = rear = NULL;
    count = 0;
	}

	void enQueue(array<string, 1000> x)
	{

		// Create a new LL node
		QNode* temp = new QNode(x);

		// If queue is empty, then
		// new node is front and rear both
		if (rear == NULL) {
			front = rear = temp;
			return;
		}

		// Add the new node at
		// the end of queue and change rear
		rear->next = temp;
		rear = temp;
    count = count + 1;
    cout << "count"<<count<<endl;
	}

	// Function to remove
	// a key from given queue q
	void deQueue()
	{
		// If queue is empty, return NULL.
		if (front == NULL)
			return;

		// Store previous front and
		// move front one node ahead
		QNode* temp = front;
		front = front->next;

		// If front becomes NULL, then
		// change rear also as NULL
		if (front == NULL)
			rear = NULL;

		delete (temp);
    count--;
	}

  int isEmpty(){
    return count == 0;
  }

  int size() {
    return count;
  }
};


struct msg {
  string data[1000];
};
// Driven Program
int main()
{
  array<string,1000> array_string1;
  for(int i=0;i<array_string1.size();i++){
    array_string1[i] = "test "+to_string(i);
  }
  cout <<array_string1.size() <<endl;
  // for(int i=0;i<array_string1.size();i++){
  //   cout << array_string1[i] <<endl;
  // }
  // array<string,1000> array_string1;
  // array_string1[0] ="sddfsd";
  //
  array<string,1000> array_string2;
  array_string2[0] = "array_string2";
  //

  Queue q;

  q.enQueue(array_string1);
  printf("size %d\n",q.size() );
  q.enQueue(array_string2);
  cout << q.size() <<endl;
  q.deQueue();
  // q.deQueue();
  // cout << q.isEmpty() <<endl;
  cout << q.size() <<endl;
  // array<string,1000> curr_node_data= (q.front)->data;
  // for(int i=0;i<curr_node_data.size();i++){
  //   cout << curr_node_data[i] <<endl;
  // }

  // cout << "Queue Front : " << (q.front)->data[0] << endl;
  cout << "Queue Front : " << (q.rear)->data[0] << endl;
}
// This code is contributed by rathbhupendra
