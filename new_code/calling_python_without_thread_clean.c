#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <time.h>

#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <Python.h>
#include </usr/include/python2.7/numpy/arrayobject.h>

#define true 1
#define BUFFER_SIZE 1024


typedef struct {
    char *can ;
}data;

int calling_python_function(data *messages)
{
    printf("This the being of the function \n");
    PyObject *pName, *pModule, *pyDict, *pFunc, *pValue, *presult, *msg ;

    Py_Initialize();
    if(!Py_IsInitialized()){
        PyErr_Print();
        printf("can't reinitialized the interperter\n");
    }

    import_array();
    PyGILState_STATE d_gstate;
    d_gstate = PyGILState_Ensure();

    PyObject *sysPath = PySys_GetObject("path");
    PyList_Append(sysPath, PyUnicode_FromString("/home/jk/Documents/Fall2021/Research_699/new_code/"));
    pName = PyUnicode_FromString((char*)"messages_correlation");
    if (!pName){
      PyErr_Print();
      printf("error locating the file name ");
    }
    pModule = PyImport_Import(pName);
    if (!pModule){
      PyErr_Print();
      printf("Error in pModule");
    }
    pyDict = PyModule_GetDict(pModule);
    pFunc = PyDict_GetItemString(pyDict, (char*)"process_can_data");


    if (PyCallable_Check(pFunc))
    {
       msg = PyList_New(1000);
       Py_ssize_t size = PyList_GET_SIZE(msg);
       for ( Py_ssize_t s = 0; s<= 999; ++s){
            PyList_SetItem(msg, s, Py_BuildValue("s", messages[s].can));
    }
        printf("Let's give this a shot!\n");
       // presult = PyObject_CallFunctionObjArgs(pFunc, msg, NULL );
        PyObject_CallFunctionObjArgs(pFunc, msg, NULL );
    }
    else
    {
       PyErr_Print();
       printf("sucess but fails");
    }
    if (pValue == NULL){
        Py_DECREF(pModule);
        Py_DECREF(pName);
        Py_DECREF(msg);
        PyErr_Print();
    }

   PyGILState_Release(d_gstate);

    //Py_Finalize();
    return 1;


}

int main()
{

    int s , r;
    int reading_data;
    struct sockaddr_can addr;
    struct ifreq interface;
    struct canfd_frame cfd;
    int x = 0;
    data f[1000];
    clock_t t;


    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
       perror("Can't connect to Socket");
       return 1;
    }

    strcpy(interface.ifr_name, "vcan0" );
    ioctl(s, SIOCGIFINDEX, &interface);
    addr.can_family = AF_CAN;
    addr.can_ifindex = interface.ifr_ifindex;
    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
                perror("Can't Bind to vcan0");
                return 1;
    }

    do {
        r  = read(s, &cfd, CANFD_MTU);
        if (r < 0)
        {
            perror("can raw socket read");
            return 0;
        }
        char * can_d = malloc(BUFFER_SIZE);
        // if(!(f[x].can = malloc(sizeof(can_d)))){
        //     printf("error allocating buffer \n");
        // }
        //printf("before appending data is = %d\n", sizeof(can_d));
        snprintf(can_d, 100, "can_id: 0x%X data length: %d data: %02X", cfd.can_id, cfd.len,  cfd.data);
        f[x].can = can_d;
        printf("%d: %s\n", x,f[x].can);
        if (x == 1000){
            t = clock();
            int g = calling_python_function(f);
            t = clock() - t;
            double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
            printf("calling the python function() took %f seconds to execute \n", time_taken);
            if (g ==-1){
                printf("Error detected\n");
             }
            else{
                printf("no problem detected\n");
            }
        // printf("%d\n",x);
        //     // for (int j =0; j <= 1000; j++){
        //     //     free(f[j].can);
        //     // }
        // //free(f->can);
        x = 0;
        printf("%d\n",x );
         }
        x++;
        }while(1);


    // closing the socket
    if (close(s) < 0) {
            perror("Unable to Close the socket ");
            return 1;
        }


    return 0;
}
