from math import *
# from scipy.stats import pearsonr
import numpy as np
import time


pid_values = ['95 ', '166', '161', '191', '164', '17c',
              '158', '133', '136', '13f', '183', '143', '13a', '18e',
              '244', '39 ' '1a4', '1dc', '1cf', '1b0', '1d0', '1aa',
              '21e', '294', '320', '324', '37c', '309', '333', '305'
              '40c', '454', '428', '405', '188', '5a1', '19b']

cosine_similarity_1 = []
pearson_simialrity_1 = []


# def main(buffer):
#     print("this the main function")
#     process_can_data(buffer)
# for i in msgs:
#     print(i)
# try:
#     thread = threading.Thread(target=message_corr, args=(msgs))
#     thread.start()
# except (KeyboardInterrupt, SystemExit):
#     cleanup_stop_thread()
#     sys.exit()
# return 1


# def message_corr(can_messages):
#     print("This the being of the function")
#     try:
#         print(
#             f"this the length of data coming out the queue {len(can_messages)}")
#         print(f"this a new message {time.time()}")
#         process_can_data(can_messages)
#     except OSError:
#         sys.exit()


def get_ID(can_frames):
    canData = []
    for row in can_frames:
        row = str(row)
        record = {'PID': row[10:13]}
        canData.append(record)
        record = {}
    return canData


def cosine_similarity(temp):
    v1 = temp[0]
    v2 = temp[1]
    "compute cosine similarity of v1 to v2: (v1 dot v2)/{||v1||*||v2||)"
    sumxx, sumxy, sumyy = 0, 0, 0
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        sumxx += x * x
        sumyy += y * y
        sumxy += x * y
    result = sumxy / sqrt(sumxx * sumyy)
    return result


def perason_corrleation(v1, v2):
    x = np.asarray(v1)
    y = np.asarray(v2)
    mx = x.mean()
    my = y.mean()
    xm, ym = x - mx, y - my
    r_num = np.add.reduce(xm * ym)
    # print(f"this the top {r_num}")
    x_r = xm * xm
    y_r = ym * ym
    # print(r_num)
    r_den = np.sqrt(np.add.reduce(x_r * y_r))
    # print(f"this the bottom value {r_den}")
    r = r_num / r_den
    return (abs(r))


def map1(unique_pid):
    pid_data1 = []
    for i in range(0, 35):
        for j in range(0, 35):
            pid_data1.append((unique_pid[i]) + (unique_pid[j]))
    return pid_data1


def map2(key_data, data, low, high):
    map3 = dict.fromkeys(key_data, 0)
    for i in range(low, high, 1):
        key = (data[i]['PID']) + (data[i + 1]['PID'])
        if key in map3:
            map3[key] += 1
        else:
            map3[key] = 1
    result = []
    for key in key_data:
        result.append(map3[key])
    return result


def get_similarities(CanData, window):

    cosine_similarity_precedence = []
    pearson_similarity_precedence = []

    N_Slices = int(len(CanData) / window) - 4
    for i in range(N_Slices):
        l1 = window * i
        l2 = window * (i + 1)
        l3 = window * (i + 2)
        l4 = window * (i + 3)

        pid_data = map1(pid_values)
        v_1 = map2(pid_data, CanData, l1, l2)
        v_2 = map2(pid_data, CanData, l2, l3)
        v_3 = map2(pid_data, CanData, l3, l4)
        v_4 = abs(np.subtract(v_2, v_1))
        v_5 = abs(np.subtract(v_3, v_2))
        cosine_similarity_precedence.append(
            round(cosine_similarity([v_1, v_2]), 2))
        corr_A = perason_corrleation(v_1, v_2) #need to import scipy
        corr_A = round(corr_A, 2)
        pearson_similarity_precedence.append(corr_A)

    return cosine_similarity_precedence, pearson_similarity_precedence


def process_can_data(can_data):
    start = time.perf_counter()
    window = 100
    message_ID = get_ID(can_data)
    # print(message_ID)

    cosine_similar, pearson_similar = get_similarities(message_ID, window)
    # cosine_similarity_1.append(cosine_similar)
    # pearson_simialrity_1.append(pearson_similar)
    print(f"this the cosine similarity {cosine_similar}")
    print(f"this the cosine average {np.average(cosine_similar)}")
    print(f"this the pearson correlation {pearson_similar}")
    # Your code that you want to time
    end = time.perf_counter() - start
    print(f"this the end of pyhthon execution {end}")

    return 1


# if __name__ == '__main__':

    # with open('data.txt') as csv_file:
    #     can_data = csv.reader(csv_file)
    #     can_data = list(can_data)
    #     for i in range(0, 17):
    #         b = i * 1000
    #         f = i + 1
    #         f = f * 1000
    #         print(b)
    #         print(f)
    #         process_can_data(can_data[b:f])
