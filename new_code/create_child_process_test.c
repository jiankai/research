#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

/**

**/
void checkStatus(pid_t process_id){
  printf("checkStatus getpid: %u ",getpid() );
  printf("process_id: %d\n", process_id);
  int status;
  pid_t return_pid = waitpid(process_id,&status, WNOHANG);

  printf("return_pid: %d\n", return_pid);
  printf("status: %d\n", status);
  // if(return_pid == 0){
  //   printf("status is requested for any process whose process group ID (GID) matches the process group ID of the caller\n" );
  //
  // }else if (return_pid > 0){
  //   printf("status is requested for a single process.\n" );
  //
  // }else if(return_pid == -1){
  //   printf("status is requested for any child process\n" );
  // }else if(return_pid < -1){
  //   printf("status is requested for any process whose process group ID is equal to the absolute value of pid.\n" );
  // } else if(return_pid == process_id) {
  //   printf("asd\n");
  // }
}

void forkexample() {
  pid_t parent_pid = getpid();
  pid_t pid;
  pid = fork();
  pid_t pidChildProcess;
  printf("parent pid %d\n",parent_pid );
  if (pid == 0) {
    printf("pid: %u ",getpid() );
    printf("Hello from Child!\n" );
    pidChildProcess = getpid();
    //sleep(4);
    //exit(0);
  } else if(pid < 0){
    printf("Create Child unsuccessful\n");
    return;

  } else {
    printf("pid: %u ",getpid() );
    printf("Hello from Parent\n" );

  }
checkStatus(pidChildProcess);
}


int main() {
  forkexample();

  return 0;
}
