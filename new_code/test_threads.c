#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <Python.h>
#include </usr/include/python2.7/numpy/arrayobject.h>
#include "../test_code/queue.h"

#define true 1
#define BUFFER_SIZE 1024


typedef struct {
    char *can ;
}data;

struct Queue* queue;



/**
 * Function to free memory
 * @param arrayOfString  [pointer to array]
 * @param size           [size of array]
 */
void free_memory(char **arrayOfString, int size){
  for(int i = 0; i < size; i++){
    free(arrayOfString[i]);
  }
  free(arrayOfString);
}

void signalHandler(int signal) {
  printf("Cought signal %d!\n", signal);
  switch (signal) {
    case SIGCHLD:
      printf("Child ended\n" );
  }
}

/**
 * reading can data and perform enqueue
 * @param  input               [description]
 * @return       [description]
 */
void *reading_can_thread(void *input) {

  long *id = (long*) input;
  printf("reading_can_thread id: %ld\n", *id);
  int s , r;
  struct sockaddr_can addr;
  struct ifreq interface;
  struct canfd_frame cfd;
  int x = 0;
  data f[1000];

  if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
     perror("Can't connect to Socket");
     return 1;
  }
  strcpy(interface.ifr_name, "vcan0" );
  ioctl(s, SIOCGIFINDEX, &interface);
  addr.can_family = AF_CAN;
  addr.can_ifindex = interface.ifr_ifindex;
  if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
              perror("Can't Bind to vcan0");
              return 1;
  }
  char **arrayOfString;
  //allocating memory for arrayOfString
  arrayOfString = malloc(1000 * sizeof(char*));
  for(int i = 0; i < 1000; i++){
    arrayOfString[i] = malloc(BUFFER_SIZE);
  }
  do {
      r  = read(s, &cfd, CANFD_MTU);
      if (r < 0)
      {
          perror("can raw socket read");
          return 0;
      }

      snprintf(arrayOfString[x], 100, "can_id: 0x%X data length: %d data: %02X", cfd.can_id, cfd.len,  cfd.data);
      // printf("test: %d: %s\n", x,can_d );
      f[x].can = arrayOfString[x];
      // printf("%d: %s\n", x,f[x].can);
      if (x == 999){ // 0 -1000
          printf("enqueuing to the queue\n" );
          enqueue(queue,arrayOfString);
          printf("enqueue size: %d\n",queue_size(queue) );
          x = 0;

          printf("---------------------%d\n",x );
          // free_memory(arrayOfString, 1000);
          // break;
      }
      x++;
      }while(1);
}

/**
 * perform dequeue when there is not empty
 * @param  input               [description]
 * @return       [description]
 */
void *thread2_test(void *input) {
  long *id = (long*) input;
  printf("reading_can_thread id: %ld\n", *id);
  struct Data current;
  int count =0;
  do {
    if(!isEmpty(queue)) {
      current = dequeue(queue);
      for(int i =0;i<1000;i++) {
        printf("count: %d -- %s\n", count,current.listOfString[i] );
      }
      printf("size: %d\n",queue_size(queue) );
      count++;
    }
  }while(1);
}
/**
 * Main
 * @return 0
 */
int main()
{

    int s , r;
    int reading_data;
    struct sockaddr_can addr;
    struct ifreq interface;
    struct canfd_frame cfd;
    int x = 0;
    data f[1000];
    clock_t t;
    pid_t pid;
    pthread_t reading_thread;
    pthread_t thread2;
    // signal(SIGCHLD,signalHandler);

    printf("Program started\n");
    queue = createQueue(1000);
    pthread_create(&reading_thread, NULL, reading_can_thread, (void *)&reading_thread);
    pthread_create(&thread2, NULL, thread2_test, (void *)&thread2);
    pthread_exit(NULL);


    // closing the socket
    if (close(s) < 0) {
            perror("Unable to Close the socket ");
            return 1;
        }


    return 0;
}
